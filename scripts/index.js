$(document).ready(function () {
    new Swiper('.slider', {
        slidesPerView: 1,
        pagination: {
            el: '.slider-pagination',
            type: 'bullets',
            clickable: true
        },
        navigation: {
            nextEl: '.slider-btn--next',
            prevEl: '.slider-btn--prev',
        }
    });
});